{
  const {
    html,
  } = Polymer;
  /**
    `<comp-operacion>` Description.

    Example:

    ```html
    <comp-operacion></comp-operacion>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --comp-operacion | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CompOperacion extends Polymer.Element {

    static get is() {
      return 'comp-operacion';
    }

    static get properties() {
      var cuenta= JSON.parse(sessionStorage.getItem("cuenta"));
      return {
        "_numeroCuenta":{
          type: String,
          value: cuenta.numero
        },
        "_saldo":{
          type: Number,
          value: cuenta.saldo
        },
        "_descripcion":{
          type: String,
          value: ""
        },
        "_valor":{
          type: Number,
          value: 0
        },
        "_idCuenta":{
          type: String,
          value: cuenta.id
        },
      };
    }



    _addMovimiento() {
      console.info('comp-operacion::_addMovimiento... begin');     
      var movimiento = {
        "idCuenta": this._idCuenta,
        "valor": this._valor,
        "descripcion": this._descripcion
      }
        
      if(movimiento.idCuenta == ''){
        return false;
      }
      if(movimiento.valor == ''){
        return false;
      }
      if(movimiento.descripcion == ''){
        return false;
      }
      this.dispatchEvent(new CustomEvent('add-movimiento', {composed: true, bubbles: true, detail: movimiento}));
      console.info('comp-lista::_addMovimiento... end');
      
    }
    _irCuentas() {
      console.info('comp-operacion::_irCuentas....')
      this.dispatchEvent(new CustomEvent('ir-cuentas', {composed: true, bubbles: true}));
    } 
    static get template() {
      return html `
      <style include="comp-operacion-styles comp-operacion-shared-styles"></style>
      <slot></slot>
      <cells-component-app-header text="Operacion: Cuenta {{_numeroCuenta}} : {{_saldo}}" icon-left="back">
      </cells-component-app-header>
      
      <p>
      <cells-molecule-input name="toggleIcons" label="ingrese descripcion" icon="coronita:search" icon-toggled="coronita:close" toggle-empty-fill-icons type="text" value={{_descripcion}}>
      </cells-molecule-input>
    </p>
    <p>
      <cells-molecule-input name="oneIcon" label="ingrese valor" icon="coronita:close" icon-label="icon-label-clear" type="text" value={{_valor}}>
      </cells-molecule-input>
    </p>
    <div>
    <cells-st-button id="btn1" class="primary">
      <button on-click="_addMovimiento">Aceptar</button>
    </cells-st-button>
    <cells-st-button id="btn2" class="primary">
      <button on-click="_irCuentas">Cancel</button>
    </cells-st-button>
  </div>
   
      
      `;
    }
  }

  customElements.define(CompOperacion.is, CompOperacion);
}